#!/bin/bash

set -e

rm -fr tmp
mkdir -p tmp

function crop_original_images() {
    # 「新」しいパーティション
    convert d-i-original.png -crop 20x20+180+145 tmp/original-new.png
    convert tmp/original-new.png -resize 60x60 tmp/original-new.2x.png

    # 「空」き領域
    convert d-i-original.png -crop 20x20+396+145 tmp/original-space.png
    convert tmp/original-space.png -resize 60x60 tmp/original-space.2x.png

    # 暗号「化」
    convert d-i-original.png -crop 20x20+63+267 tmp/original-encryption.png
    convert tmp/original-encryption.png -resize 60x60 tmp/original-encryption.2x.png

    # 「終」了
    convert d-i-original.png -crop 20x20+158+462 tmp/original-finish.png
    convert tmp/original-finish.png -resize 60x60 tmp/original-finish.2x.png

    # 「戻」る
    convert d-i-original.png -crop 20x20+575+566 tmp/original-back.png
    convert tmp/original-back.png -resize 60x60 tmp/original-back.2x.png

    # 「続」ける
    convert d-i-original.png -crop 20x20+702+564 tmp/original-continue.png
    convert tmp/original-continue.png -resize 60x60 tmp/original-continue.2x.png
}

function crop_fixed_images() {
    # 「新」しいパーティション
    convert d-i-fixed.png -crop 20x20+180+137 tmp/fixed-new.png
    convert tmp/fixed-new.png -resize 60x60 tmp/fixed-new.2x.png

    # 「空」き領域
    convert d-i-fixed.png -crop 20x20+396+137 tmp/fixed-space.png
    convert tmp/fixed-space.png -resize 60x60 tmp/fixed-space.2x.png

    # 暗号「化」
    convert d-i-fixed.png -crop 20x20+63+243 tmp/fixed-encryption.png
    convert tmp/fixed-encryption.png -resize 60x60 tmp/fixed-encryption.2x.png

    # 「終」了
    convert d-i-fixed.png -crop 20x20+158+425 tmp/fixed-finish.png
    convert tmp/fixed-finish.png -resize 60x60 tmp/fixed-finish.2x.png

    # 「戻」る
    convert d-i-fixed.png -crop 20x20+575+566 tmp/fixed-back.png
    convert tmp/fixed-back.png -resize 60x60 tmp/fixed-back.2x.png

    # 「続」ける
    convert d-i-fixed.png -crop 20x20+702+564 tmp/fixed-continue.png
    convert tmp/fixed-continue.png -resize 60x60 tmp/fixed-continue.2x.png
}

CHAR_FILES="
tmp/new.png
tmp/space.png
tmp/encryption.png
tmp/finish.png
tmp/back.png
tmp/continue.png
"
case $1 in
    fixed)
	crop_fixed_images
	;;
    original)
	crop_original_images
	;;
    *)
	crop_fixed_images
	crop_original_images
	convert -size 60x60 xc:#ffffff -gravity center -fill black -stroke black -draw "text 0,0 Before" tmp/before.png
	convert -size 60x60 xc:#ffffff -gravity center -fill black -stroke black -draw "text 0,0 After" tmp/after.png

	convert +append tmp/before.png tmp/after.png tmp/before-after.png
	for d in new space encryption finish back continue; do
	    convert +append tmp/original-$d.2x.png tmp/fixed-$d.2x.png tmp/$d.png
	done
	convert -append tmp/before-after.png $CHAR_FILES d-i-compared.png
	;;
esac
