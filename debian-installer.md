# Debian Installerの\\n日本語フォントを\\nどうにかする話

subtitle
:  日本語を選択して\\n違和感なくインストールしたい

author
:  Kentaro Hayashi

institution
:  ClearCode Inc.

content-source
:  2023年7月 東京エリア・関西合同Debian勉強会

allotted-time
:  20m

theme
:  .

# スライドはRabbit Slide Showにて公開済みです

* Debian Installerの日本語フォントをどうにかする話
  * <https://slide.rabbit-shocker.org/authors/kenhys/tokyodebian-d-i-font-202307>

# 本日の内容

* Debian Installerを改善する試み
  * 日本語を選択してインストールするときの違和感
  * 違和感の正体とその経緯について
  * どうすればいいのか？
  * Debian Installerのビルドをして検証してみた

# Debian Installerとは

* <https://www.debian.org/devel/debian-installer/index.en.html>
  * Debianをインストールするためのイメージを提供
  * ネットワークインストールしたり、ライブイメージも利用できる
  * 詳細は <https://d-i.debian.org/> 参照

# Debian Installerのドキュメント

* Debian Installer Internationalization and Localization Guide
  * <https://d-i.debian.org/doc/i18n-guide/>
* Debian Installation Guide — Development version
  * <https://d-i.debian.org/doc/installation-guide/>
* Debian Installer internals
  * <https://d-i.debian.org/doc/internals/>

# インストール中に\\n違和感を覚える

* インストール時に日本語🇯🇵を選択すると発生
* インストールそのものに支障をきたすほどではない🤔

# Debian bookwormのインストーラーの画面

![](images/d-i-original.png){:relative-height="100"}

# 間違い探しの答え

![](images/d-i-highlight.png){:relative-height="100"}

# どういうことか？

* 一部の文字がいわゆる中華フォントになる
  * 「新」、「空」、「化」、「戻」、「続」など❌
  * フォントが中華フォント臭いなと思いながらもインストールをそのまま続行してしまいがち

# 問題の発生する環境

* Debian 8 (jessie) では発生しない
  * 2015/04/25-2018/06/17 EOL, 2020/06/30 EOL LTS
* Debian 9 (stretch) 以降で発生する
  * 2017/06/17-2020/07/18 EOL, 2022/07/01 EOL LTS

# Debian 9で\\n何が起こったのか

* Proposal: Drop ttf-cjk-compact, get fonts-droid
  * <https://lists.debian.org/debian-boot/2015/07/msg00304.html>
  * ttf-cjk-compactではなく fonts-androidが採用された
  * ttf-cjk-compactでは必要な文字を含むコンパクトなフォントを用意していた
    * ~ 160KB程度にまとめていた

# fonts-android?

* <https://wiki.debian.org/DebianInstaller/GUIFonts>
  * CJK向けフォントとしてfonts-androidではDroidSansFallbackが採用
  * 結果として特定の文字が中華フォントになる Han Unification

# 変更当時から\\n問題が認識されていた

* kmutoさん曰く

      - Some Japanese characters look bit funny.
      (I believe it is negligible for installer.)

  * <https://lists.debian.org/debian-boot/2015/07/msg00304.html>

# Han Unificationに関する問題の一般的な解説記事

* 参考: Your Code Displays Japanese Wrong
  * <https://heistak.github.io/your-code-displays-japanese-wrong/>

# Your Code Displays Japanese Wrong

![](images/heistack.png){:relative-height="75"}

* <https://heistak.github.io/your-code-displays-japanese-wrong/>

# どうするとよいのか

* ざっくりいうと
  * 適切なフォントを選んで
  * 言語選択時に変更する

# 適切なフォントを選ぶ

* 案1: fonts-androidにフォントを追加する
  * ソースアーカイブ(MTLc3m.ttf)にのみあり、インストールはされない
  * MTLc3m.ttfは1.9MBほど(他の日本語フォントと比べるとコンパクト)
  * fonts-android.udeb**のみ**フォントを追加インストールするよう変更

# 適切なフォントを選ぶ(2)

* 案2: fonts-motoya-l-cedarのudebを用意する
  * udebを提供するのは理にかなっている

# どうするとよいのか

* 適切なフォントを選んで
  * **fonts-motoya-l-cedarのudebを用意**
* 言語選択時に変更する

# udeb?

* 拡張子が.udebでmicro-debと呼称
* d-i専用の容量削減を重視するdebパッケージ
  * Debian Policyのもろもろには準拠せず
    * アンインストールや更新は意図されていない
    * changelogやライセンス、ドキュメント等を省略
  * See Chapter 3. D-I components or udebs
    * <https://d-i.debian.org/doc/internals/ch03.html>

# 言語選択時に変更する(1)

* d-iは各種パッケージから構成される

![](images/d-i-packages.png){:relative-height="85" :relative-width="100"}

# 言語選択時に変更する(2)

* cdefconfでGTKのリソースを変更(抜粋)
  * たぶんもっとよいやりかたはあるはず

```
static void set_language_specific_font_name(struct frontend *fe)
char * language = cdebconf_gtk_get_text(fe, "debconf/language",
                                        "Current language for installer");
if (language && strcmp(language, "ja") == 0) {
  gtk_rc_parse_string('gtk-font-name = "MotoyaLCedar"');
}
```

# どうするとよいのか

* 適切なフォントを選んで
  * **fonts-motoya-l-cedarのudebを用意**
* 言語選択時に変更する
  * **cdefconfでGTKのリソースを変更**
    * 注: もっとよいやりかたはありそう

# インストーラーを\\nビルドして検証する

* Debian Installerのビルド方法を解説

# Debian Installerのビルド方法(1)

* リポジトリのチェックアウト

```
$ sudo apt install -y myrepos
$ mr bootstrap \
  https://salsa.debian.org/installer-team/d-i/raw/master/.mrconfig \
  debian-installer
```

# Debian Installerのビルド方法(2)

* 必要なパッケージのインストール
  * パッケージいろいろインストールするのでコンテナ推奨

```
# apt install -y \
  git curl myrepos dpkg-dev \
  apt-utils wget bc xsltproc docbook-xml docbook-xsl libbogl-dev \
  genext2fs genisoimage dosfstools cpio \
  syslinux syslinux-utils isolinux pxelinux syslinux-common \
  shim-signed grub-efi-amd64-signed grub-common xorriso tofrodos mtools kmod \
  bf-utf-source win32-loader librsvg2-bin fdisk fontconfig
```

# Debian Installerのビルド方法(3)

* フォントのudebを用意する
  * debian/control

```
diff -ur fonts-motoya-l-cedar-1.01.orig/debian/control fonts-motoya-l-cedar-1.01/debian/control
--- fonts-motoya-l-cedar-1.01.orig/debian/control       2019-04-14 22:16:41.000000000 +0900
+++ fonts-motoya-l-cedar-1.01/debian/control    2023-07-03 18:44:45.938298199 +0900
@@ -21,3 +21,14 @@
  .
  This package provides "MotoyaLCedar W3 mono" - Gothic that images Japanese 
  cedar, straight and thick tree.
+
+Package: fonts-motoya-l-cedar-udeb
+Package-Type: udeb
+Section: debian-installer
+Architecture: all
+Description: Japanese Truetype font, Motoya L Cedar
```

# Debian Installerのビルド方法(4)

* MTLc3m.ttfをインストールするための.installを用意
  * debian/fonts-motoya-l-cedar-1.01/debian/fonts-motoya-l-cedar-udeb.install

```
MTLc3m.ttf      usr/share/fonts/truetype/motoya-l-cedar/
```

# Debian Installerのビルド方法(5)

* udebを参照できるようにする
  * debian-installer/installer/build/sources.list.udeb.localを配置する
    * 例: /debianがコンテナにマウントしたdebian-installerディレクトリ

```
deb [trusted=yes] copy:/debian/installer/build localudebs/
deb http://deb.debian.org/debian unstable main/debian-installer
```

# Debian Installerのビルド方法(6)

* debian-installer/installer/build/localudebsにフォントのudebを配置する

```
# ls -1 /debian/installer/build/localudebs/
Packages
Packages.gz
fonts-motoya-l-cedar-udeb_1.01-5.1_all.udeb
```

# Debian Installerのビルド方法(7)

* installer/build/pkg-lists/gtk-commonのパッケージリストにフォントを追加する

```
# For Japanese
fonts-motoya-l-cedar-udeb
```

# Debian Installerのビルド方法(8)

* cdebconfにパッチをあてる
  * packages/cdebconf/src/modules/frontend/gtk/di.c

```
diff --git a/src/modules/frontend/gtk/di.c b/src/modules/frontend/gtk/di.c
index 3dc5d38c..e85277a3 100644
--- a/src/modules/frontend/gtk/di.c
+++ b/src/modules/frontend/gtk/di.c
@@ -321,6 +321,30 @@ static GtkTextDirection get_text_direction(struct frontend * fe)
     return direction;
 }

+/** Set language specific gtk-font-name explicitly
+ *
+ * @param fe
+ */
+static void set_language_specific_font_name(struct frontend *fe)
+{
+    char * language = cdebconf_gtk_get_text(fe, "debconf/language",
+                                            "Current language for installer");
+    if (language && strcmp(language, "ja") == 0) {
+        /* font-android-udeb is used for CJK, but because of Han unification,
+         * some of font typefaces are rendered as Simplified Chinese, not
+         * Japanese.
+         * This issue is not solved by using DroidSansFallback.ttf
+         * (fonts-android-udeb), thus another font which contains
+         * Japanese font typeface must be used for Japanese.
+         *
+         * MotoyaLCedar is suitable font which can be bundled for
+         * fonts-android-udeb.
+         * [1] https://wiki.debian.org/DebianInstaller/GUIFonts
+         */
+        gtk_rc_parse_string('gtk-font-name = "MotoyaLCedar"');
+    }
+}
+
 /** Update various settings with the current language settings.
  *
  * @param fe cdebconf frontend
@@ -331,6 +355,9 @@ static void refresh_language(struct frontend * fe)
     gtk_rc_reparse_all();
     /* Adapt text direction. */
     gtk_widget_set_default_direction(get_text_direction(fe));
+
+    /* Override specific font name */
+    set_language_specific_font_name(fe);
 }
```

# Debian Installerのビルド方法(9)

* build/config/commonでLINUX_KERNEL_ABIを環境にあわせて設定しなおす
  * linux-image-x.y.z-1が揃っていないと依存するudebをダウンロードできない

```
# Default kernel ABI version to use. Append a kernel flavour to
# produce KERNELVERSION.
#LINUX_KERNEL_ABI ?= 6.3.0-2
LINUX_KERNEL_ABI ?= 6.1.0-9
```

# Debian Installerのビルド方法(10)

* buildディレクトリに移動して、build_netboot-gtkをビルド

```
# cd /debian/installer/build/
# make build_netboot-gtk
```

# Debian Installerのビルド方法(11)

* ビルドが成功するとmini.isoができる

```
/debian/installer/build/dest/netboot/gtk/mini.iso
```

# 手元で修正してみた画面

![](images/d-i-fixed.png){:relative-height="100"}


# 新旧の比較

* 中華フォントが修正されている

![](images/d-i-compared.png){:relative-height="100" :relative-width="100"}

# バグレポート

* debian-installer: GUI font for Japanese was incorrectly rendered
  * <https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1037256>

* Proposal: change Japanese font for GUI installer
  * <https://lists.debian.org/debian-boot/2023/06/msg00224.html>

# さいごに

* ❌Debian 9以降インストーラーは中華フォント問題(Han Unification)を抱えている
* ✅日本語向けには次の変更で解決できる
  * フォントを追加インストールする
  * 追加インストールしたフォントを動的に選択可能にする
